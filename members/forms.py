# -*- coding: utf-8 -*-

# Standard library imports
from __future__ import unicode_literals

from django import forms
from django.contrib.auth.forms import AuthenticationForm as DjangoAuthenticationForm

class AuthenticationForm(DjangoAuthenticationForm):
    username = forms.CharField(label="Nom d'utilisateur",
                               max_length=254,
                               widget=forms.TextInput(attrs={"class": "form-control",
                                                             "placeholder": "Nom d'utilisateur"}))
    password = forms.CharField(label="Mot de passe",
                               widget=forms.PasswordInput(attrs={"class": "form-control",
                                                                 "placeholder": "Mot de passe"}))

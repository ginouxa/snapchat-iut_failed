# -*- coding: utf-8 -*-

# Standard library imports
from __future__ import unicode_literals
from django.conf.urls import patterns, url


urlpatterns = patterns('',
                       url(r'^se-deconnecter/$', 'django.contrib.auth.views.logout', {'next_page': "/"}, name="logout"))
